function reduce(array, callback, initialValue) {
  let startIndex = 0;

  if (initialValue === undefined) {
    initialValue = array[0];
    startIndex = 1;
  }

  let accumulator = initialValue;

  for (let i = startIndex; i < array.length; i++) {
    accumulator = callback(accumulator, array[i]);
  }

  return accumulator;
}
